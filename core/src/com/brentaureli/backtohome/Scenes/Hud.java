package com.brentaureli.backtohome.Scenes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.brentaureli.backtohome.BackToHome;

public class Hud implements Disposable {

    //Scene2D.ui Stage and its own Viewport for HUD
    public Stage stage;
    private Viewport viewport;

    //Time Tracking Variables
    private int worldTimer;
    private float timeCount;

    //Scene2D widgets
    Label countdownLabel;
    Label timeLabel;


    public Hud(SpriteBatch sb){
        //define tracking variables
        worldTimer = 300;
        timeCount = 0;

        //setup the HUD viewport using a new camera seperate from  gamecam
        //define our stage using that viewport and  games spritebatch
        viewport = new FitViewport(BackToHome.V_WIDTH,BackToHome.V_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport, sb);

        //define a table used to organize our hud's labels
        Table table = new Table();
        //Top-Align table
        table.top();
        //make the table fill the entire stage
        table.setFillParent(true);

        //define labels using the String, and a Label style consisting of a font and color
        countdownLabel = new Label(String.format("%03d", worldTimer), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        timeLabel = new Label("TIME", new Label.LabelStyle(new BitmapFont(), Color.WHITE));

        //add  label to  table, padding the top, and giving them all equal width with expandX
        table.add(timeLabel).expandX().padTop(10);
        //add a second row to table
        table.row();
        table.add(countdownLabel).expandX();

        //add our table to the stage
        stage.addActor(table);
    }

    public void update(float dt){
        timeCount += dt;
        if(timeCount >=1){
            worldTimer--;
            countdownLabel.setText(String.format("%03d", worldTimer));
            timeCount = 0;
        }

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
