package com.brentaureli.backtohome.Sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.utils.Array;
import com.brentaureli.backtohome.BackToHome;
import com.brentaureli.backtohome.Screens.PlayScreen;

public class Bomb1 extends Enemy{

    private float stateTime;
    private Animation<TextureRegion> walkAnimation;
    private Array<TextureRegion> frames;


    public Bomb1(PlayScreen screen, float x, float y) {
        super(screen, x, y);
        frames = new Array<TextureRegion>();
        frames.add(new TextureRegion(screen.getAtlas().findRegion("Bomb1") , 139 ,152,45,40));
        walkAnimation = new Animation(0.4f,frames);
        stateTime = 0;
        setBounds(getX(),getY(),46/ BackToHome.PPM ,46/ BackToHome.PPM);
    }

    public  void  update(float dt){
        stateTime += dt;
        b2body.setLinearVelocity(velocity);
        setPosition(b2body.getPosition().x - getWidth() / 2, b2body.getPosition().y - getHeight() / 2);
        setRegion(walkAnimation.getKeyFrame(stateTime, true));
    }

    @Override
    protected void defineEnemy() {
        BodyDef bdef = new BodyDef();
        bdef.position.set(getX(), getY());
        bdef.type = BodyDef.BodyType.DynamicBody;
        b2body = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(5 / BackToHome.PPM);
        fdef.filter.categoryBits = BackToHome.ENEMY_BIT;
        fdef.filter.maskBits = BackToHome.GROUND_BIT | BackToHome.CHARACTER_BIT|
                BackToHome.OBJECT_BIT;

        fdef.shape = shape;
        b2body.createFixture(fdef);


    }







}
