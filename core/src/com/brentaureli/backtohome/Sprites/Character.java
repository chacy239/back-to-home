package com.brentaureli.backtohome.Sprites;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.brentaureli.backtohome.BackToHome;
import com.brentaureli.backtohome.Screens.PlayScreen;

public class Character extends Sprite {
    public enum State { STANDING, DEAD };
    public State currentState;
    public State previousState;
    public World world;
    public Body b2body;

    private float stateTimer;
    private  TextureRegion characterStand;
    private  TextureRegion characterDead;

    private boolean characterIsDead;

    private PlayScreen screen;





    public Character(World world, PlayScreen screen) {
        //initialize default values
        this.screen = screen;
        this.world = screen.getWorld();
        currentState = State.STANDING;
        previousState = State.STANDING;
        stateTimer = 0;

        Array<TextureRegion> frames = new Array<TextureRegion>();


        characterStand = new TextureRegion(screen.getAtlas().findRegion("Character") , 89, -5, 45, 50);
        defineCharacter();
        setBounds(712, 208, 50 / BackToHome.PPM, 50 / BackToHome.PPM);
        setRegion(characterStand);

    }

    public void update(float dt){
        //update sprite to correspond with the position of Box2D body
        setPosition(b2body.getPosition().x - getWidth() / 2, b2body.getPosition().y - getHeight() / 2);
    }

    public TextureRegion getFrame(float dt) {
        //get player current state.
        currentState = getState();

        TextureRegion region;

        //depending on the state, get corresponding animation keyFrame.
        switch (currentState) {
            case DEAD:
                region = characterDead;
                break;
            case STANDING:
            default:
                region = characterStand;
                break;
        }

        stateTimer = currentState == previousState ? stateTimer + dt : 0;
        //update previous state
        previousState = currentState;
        //return our final adjusted frame
        return region;

    }

    public State getState() {
        //Test to Box2D for velocity on the X and Y-Axis
        if (characterIsDead)
            return State.DEAD;
        else
            return State.STANDING;

        }

    public boolean isDead(){
        return characterIsDead;
    }

    public float getStateTimer(){
        return stateTimer;
    }

    public void die(){
        if (!isDead()) {
            BackToHome.manager.get("music/background.ogg", Music.class).stop();
            BackToHome.manager.get("sounds/bomb.wav", Sound.class).play();
            characterIsDead = true;
            Filter filter = new Filter();
            filter.maskBits = BackToHome.NOTHING_BIT;
            for (Fixture fixture : b2body.getFixtureList())
                fixture.setFilterData(filter);
        }
        b2body.applyLinearImpulse(new Vector2(0, 4f), b2body.getWorldCenter(),true);
    }

    public void hit(){
        die();
    }



    public void defineCharacter() {
        BodyDef bdef = new BodyDef();
        bdef.position.set(32 / BackToHome.PPM, 32 / BackToHome.PPM);
        bdef.type = BodyDef.BodyType.DynamicBody;
        b2body = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6 / BackToHome.PPM);
        fdef.filter.categoryBits = BackToHome.CHARACTER_BIT;
        fdef.filter.maskBits = BackToHome.GROUND_BIT |
                BackToHome.ENEMY_BIT|
                BackToHome.OBJECT_BIT;

        fdef.shape = shape;
        b2body.createFixture(fdef);

        EdgeShape head = new EdgeShape();
        head.set(new Vector2(-2 / BackToHome.PPM, 7 / BackToHome.PPM), new Vector2(2 / BackToHome.PPM, 7 / BackToHome.PPM));
        fdef.shape = head;
        fdef.isSensor = true;

        b2body.createFixture(fdef).setUserData("head");
    }

    public void draw(Batch batch) {
        super.draw(batch);

    }



}
