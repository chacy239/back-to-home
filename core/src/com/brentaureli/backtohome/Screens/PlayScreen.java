package com.brentaureli.backtohome.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.Map;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.brentaureli.backtohome.BackToHome;
import com.brentaureli.backtohome.Scenes.Hud;
import com.brentaureli.backtohome.Sprites.Bomb1;
import com.brentaureli.backtohome.Sprites.Character;

import com.brentaureli.backtohome.Sprites.Enemy;
import com.brentaureli.backtohome.Tools.B2WorldCreator;
import com.brentaureli.backtohome.Tools.WorldContactListener;

public class PlayScreen implements Screen {
    //Reference to our Game, used to set Screens
    private BackToHome game;
    private TextureAtlas atlas;

    //basic playscreen variables
    private OrthographicCamera gamecam;
    private Viewport gamePort;
    private Hud hud;

    //Tiled map variables
    private TmxMapLoader maploader;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;

    //Box2d variables
    private World world;
    private Box2DDebugRenderer b2dr;
    private B2WorldCreator creator;

    //sprites
    private Character player;
    private Bomb1 bomb1;

    private Music music;


    public PlayScreen(BackToHome game) {
        atlas = new TextureAtlas("Character_and_Enemies.pack");

        this.game = game;
        //create cam used to follow player through cam world
        gamecam = new OrthographicCamera();

        //create a FitViewport to maintain virtual aspect ratio despite screen size
        gamePort = new FitViewport(BackToHome.V_WIDTH / BackToHome.PPM, BackToHome.V_HEIGHT / BackToHome.PPM, gamecam);

        //create game HUD for timers info
        hud = new Hud(game.batch);

        //Load map and setup map renderer
        maploader = new TmxMapLoader();
        map = maploader.load("firstmap.tmx");
        renderer = new OrthogonalTiledMapRenderer(map, 1 / BackToHome.PPM);

        //initially set our gamcam to be centered correctly at the start of of map
        gamecam.position.set(gamePort.getScreenWidth() / 2, gamePort.getScreenHeight() / 2, 0);

        //create Box2D world, setting no gravity in X, -10 gravity in Y, and allow bodies to sleep
        world = new World(new Vector2(0, -10), true);
        //allows for debug lines of our box2d world.
        b2dr = new Box2DDebugRenderer();

        creator = new B2WorldCreator(this);

        //create player in game world
        player = new Character(world, this);

        world.setContactListener(new WorldContactListener());

        music = BackToHome.manager.get("music/background.ogg", Music.class);
        music.setLooping(true);
        music.play();


    }

    public TextureAtlas getAtlas() {
        return atlas;
    }

    @Override
    public void show() {

    }


    public void handleInput(float dt) {
        if(player.currentState != Character.State.DEAD) {
            if (Gdx.input.isKeyJustPressed(Input.Keys.UP))
                player.b2body.applyLinearImpulse(new Vector2(0, 4f), player.b2body.getWorldCenter(), true);
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) && player.b2body.getLinearVelocity().x <= 2)
                player.b2body.applyLinearImpulse(new Vector2(0.1f, 0), player.b2body.getWorldCenter(), true);
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT) && player.b2body.getLinearVelocity().x >= -2)
                player.b2body.applyLinearImpulse(new Vector2(-0.1f, 0), player.b2body.getWorldCenter(), true);
        }

    }


    public void update(float dt) {
        //handle user input first
        handleInput(dt);

        //takes 1 step in the physics simulation(60 times per second)
        world.step(1 / 60f, 6, 2);

        player.update(dt);
        for (Enemy enemy : creator.getEnemies())
            enemy.update(dt);
        hud.update(dt);

        if (gameOver()) {
            game.setScreen(new GameOverScreen(game));
            dispose();
        }

        //attach gamecam to our players.x coordinate
        gamecam.position.x = player.b2body.getPosition().x;

        //update gamecam with correct coordinates after changes
        gamecam.update();
        //tell renderer to draw only what camera can see in game world.
        renderer.setView(gamecam);
    }


    @Override
    public void render(float delta) {
        //separate update logic from render
        update(delta);

        //Clear the game screen with Black
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //render game map
        renderer.render();

        //renderer Box2DDebugLines
        b2dr.render(world, gamecam.combined);

        game.batch.setProjectionMatrix(gamecam.combined);
        game.batch.begin();
        player.draw(game.batch);
        for (Enemy enemy : creator.getEnemies())
            enemy.draw(game.batch);
        game.batch.end();

        //Set batch to now draw what the Hud camera sees.
        game.batch.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();

        if(player.currentState != Character.State.DEAD) {
            gamecam.position.x = player.b2body.getPosition().x;
        }
    }

    public boolean gameOver(){
        if(player.currentState == Character.State.DEAD && player.getStateTimer() > 3){
            return true;
        }
        return false;
    }

    @Override
    public void resize(int width, int height) {
        //updated game viewport
        gamePort.update(width, height);

    }

    public TiledMap getMap(){
        return map;
    }
    public World getWorld(){
        return world;
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        //dispose of all our opened resources
        map.dispose();
        renderer.dispose();
        world.dispose();
        b2dr.dispose();
        hud.dispose();
    }
}