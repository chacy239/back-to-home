package com.brentaureli.backtohome.Tools;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.brentaureli.backtohome.BackToHome;
import com.brentaureli.backtohome.Screens.PlayScreen;
import com.brentaureli.backtohome.Sprites.Bomb1;
import com.brentaureli.backtohome.Sprites.Bomb2;
import com.brentaureli.backtohome.Sprites.Enemy;

public class B2WorldCreator {
    private Array<Bomb1> bomb1;
    private Array<Bomb2> bomb2;

    public B2WorldCreator(PlayScreen screen){
        World world = screen.getWorld();
        TiledMap map = screen.getMap();
        //create body and fixture variables
        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();
        Body body;

        //create ground bodies/fixtures
        for(MapObject object : map.getLayers().get(2).getObjects().getByType(RectangleMapObject.class)){
            Rectangle rect = ((RectangleMapObject)object).getRectangle();

            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set((rect.getX() + rect.getWidth() / 2) / BackToHome.PPM, (rect.getY() + rect.getHeight() / 2) / BackToHome.PPM);

            body = world.createBody(bdef);

            shape.setAsBox(rect.getWidth() / 2/ BackToHome.PPM, rect.getHeight() / 2 / BackToHome.PPM);
            fdef.shape = shape;
            body.createFixture(fdef);
        }

        //create Block bodies/fixtures
        for(MapObject object : map.getLayers().get(3).getObjects().getByType(RectangleMapObject.class)){
            Rectangle rect = ((RectangleMapObject)object).getRectangle();

            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set((rect.getX() + rect.getWidth() / 2) / BackToHome.PPM, (rect.getY() + rect.getHeight() / 2) / BackToHome.PPM);

            body = world.createBody(bdef);

            shape.setAsBox(rect.getWidth() / 2/ BackToHome.PPM, rect.getHeight() / 2 / BackToHome.PPM);
            fdef.shape = shape;
            fdef.filter.categoryBits = BackToHome.OBJECT_BIT;
            body.createFixture(fdef);


        }

        //create all bomb

        bomb1 = new Array<Bomb1>();
        for(MapObject object : map.getLayers().get(4).getObjects().getByType(RectangleMapObject.class)){
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bomb1.add(new Bomb1(screen, rect.getX() / BackToHome.PPM, rect.getY() / BackToHome.PPM));
        }

        bomb2 = new Array<Bomb2>();
        for(MapObject object : map.getLayers().get(5).getObjects().getByType(RectangleMapObject.class)){
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bomb2.add(new Bomb2(screen, rect.getX() / BackToHome.PPM, rect.getY() / BackToHome.PPM));
        }

    }

    public Array<Enemy> getEnemies() {
        Array<Enemy> enemies = new Array<Enemy>();
        enemies.addAll(bomb1);
        enemies.addAll(bomb2);
        return enemies;
    }
}
